require("@babel/register");

import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { ServerLocation } from "@reach/router"
import { ServerStyleSheet, StyleSheetManager } from 'styled-components'

// Utilities
import apiRouter from 'Routes/api';
import App from 'Components/App';

// Variables
const publicDirectory = path.join(__dirname, '../public');
const sheet = new ServerStyleSheet();

const getHtml = (req) => {
  const html = renderToString(
    <ServerLocation url={req.url}>
      <StyleSheetManager sheet={sheet.instance}>
        <App />
      </StyleSheetManager>
    </ServerLocation>
  );

  return {
    html,
    styleTags: sheet.getStyleTags(),
  };
};

const app = express();

// view engine setup
app.set('views', publicDirectory);
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(publicDirectory));

app.use('/api', apiRouter);

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
  const dynamicData = getHtml(req);

  res.render('index', {
    title: 'WHOAH! - SSR',
    reactContent: dynamicData.html,
    styleTags: dynamicData.styleTags,
  });
  // res.sendFile(path.join(publicDirectory, 'index.html'));
});

module.exports = app;
