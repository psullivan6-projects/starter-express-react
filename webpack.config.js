const webpack = require('webpack');
const path = require('path');

// Utilities
const aliases = require('./webpack/aliases')(__dirname);
const WebpackNotifierPlugin = require('webpack-notifier');


// Variables
const isDevMode = process.env.NODE_ENV === 'production' ? false : true;

module.exports = {
  mode    : isDevMode ? 'development' : 'production',
  entry   : './app/main.js',
  output  : {
    path     : path.resolve(__dirname, 'public/js'),
    filename : '[name].js',
  },
  devtool : isDevMode ? 'eval' : false,
  module  : {
    rules: [
      {
        test    : /\.(js|jsx)$/,
        exclude : /node_modules/,
        loader  : 'babel-loader',
      }
    ]
  },
  plugins: [
    new WebpackNotifierPlugin({ alwaysNotify: true }),
  ],
  resolve: {
    alias      : aliases.app,
    extensions : ['.js', '.jsx']
  },
};
