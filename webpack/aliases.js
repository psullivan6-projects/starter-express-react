const app = directory => ({
  Components : `${directory}/app/components/`,
  Data       : `${directory}/app/data/`,
  NPM        : `${directory}/node_modules/`,
  Pages      : `${directory}/app/pages/`,
  Services   : `${directory}/app/services/`,
  Styles     : `${directory}/app/styles/`,
  Utilities  : `${directory}/app/utilities/`,
});

const server = directory => ({
  Routes : `${directory}/server/routes/`,
});

module.exports = directory => ({
  app: app(directory),
  server: server(directory),
})
