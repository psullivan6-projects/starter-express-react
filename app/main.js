import React from 'react';
import { hydrate } from 'react-dom';

// Components
import App from 'Components/App';

// Variables
const root = document.getElementById('root');

hydrate(<App />, root);
