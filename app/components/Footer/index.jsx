import React, { Component } from 'react';

// Styles
import { Container, StyledLink } from './styles';

// Variables
const random = 123123123; // if this is Math.random then server and client mismatch

class Footer extends Component {
  componentDidMount() {
    console.log('Footer MOUNTED');
  }

  render() {
    return (
      <Container>
        <nav>
          <StyledLink to="/">Home</StyledLink> |{" "}
          <StyledLink to="/user">User Page</StyledLink> |{" "}
          <StyledLink to={`/user/${random}`}>Specific User Page</StyledLink>
        </nav>
      </Container>
    )
  }
}

export default Footer;
