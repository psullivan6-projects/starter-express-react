import styled from 'styled-components';
import { Link } from '@reach/router';

export const Container = styled.footer`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  padding: 2rem;
  background-color: rebeccapurple;
`;

export const StyledLink = styled(Link)`
  color: white;
  font-weight: bold;

  &:hover {
    color: red;
  }
`;
