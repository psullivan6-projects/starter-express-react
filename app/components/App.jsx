import React, { Component } from 'react';
import { Router } from '@reach/router';

// Routes
import routes from '../routes';

import Page from 'Components/Page';


export default () => (
  <Router>
    <Page path='/'>
      {
        routes.map(({ Component, path, slug, getComponent }) => {
          if (getComponent != null) {
            return getComponent();
          }

          return <Component key={slug} path={path} />;
        })
      }
    </Page>
  </Router>
);
