import styled from 'styled-components';

export const Container = styled.div`
  padding: 1rem;
  background-color: #333;
  color: white;
`;
