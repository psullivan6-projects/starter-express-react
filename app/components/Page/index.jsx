import React from 'react';

// Components
import Footer from 'Components/Footer';

// Styles
import { Container } from './styles';


export default (props) => (
  <Container>
    { props.children }
    <Footer />
  </Container>
);
