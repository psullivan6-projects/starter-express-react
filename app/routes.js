import React from 'react';

// Pages
import Home from 'Pages/home';
import User from 'Pages/user';

export default [
  {
    slug: 'home',
    path: '/',
    Component: Home
  },
  {
    slug: 'user',
    path: '/user/:pid',
    Component: User
  },
  {
    getComponent: () => (<User key='user' path='/user' />)
  }
];
