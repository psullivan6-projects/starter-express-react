import React, { Component, Fragment } from 'react';


class Home extends Component {
  componentDidMount() {
    console.log('HOMEPAGE MOUNT', this.props);
  }

  render() {
    return (
      <Fragment>
        <h1>HOMEPAGE HERE</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut itaque deserunt laborum, aspernatur eaque officiis ab obcaecati. Ad eveniet facere ipsam ipsa in labore, incidunt, hic sequi, debitis nostrum tempore.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut itaque deserunt laborum, aspernatur eaque officiis ab obcaecati. Ad eveniet facere ipsam ipsa in labore, incidunt, hic sequi, debitis nostrum tempore.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut itaque deserunt laborum, aspernatur eaque officiis ab obcaecati. Ad eveniet facere ipsam ipsa in labore, incidunt, hic sequi, debitis nostrum tempore.</p>
      </Fragment>
    )
  }
}

export default Home;
