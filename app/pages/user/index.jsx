import React, { Component, Fragment } from 'react';

// Components
import Page from 'Components/Page';


class User extends Component {
  state = {
    data: {}
  }

  componentDidMount() {
    console.log('USER PAGE MOUNT', this.props);

    fetch('/api/data')
      .then(response => response.json())
      .then(data => {
        console.log('GAWT THE DATA, YEEEEHAW', data);
        this.setState({ data });
      })
      .catch(error => {
        console.log('ERROR', error);
      })
  }

  render() {
    return (
      <Fragment>
        <h1>USER PAGE HERE</h1>
        {
          this.props.pid && (
            <h2>PID PROP: { this.props.pid }</h2>
          )
        }
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut itaque deserunt laborum, aspernatur eaque officiis ab obcaecati. Ad eveniet facere ipsam ipsa in labore, incidunt, hic sequi, debitis nostrum tempore.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut itaque deserunt laborum, aspernatur eaque officiis ab obcaecati. Ad eveniet facere ipsam ipsa in labore, incidunt, hic sequi, debitis nostrum tempore.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut itaque deserunt laborum, aspernatur eaque officiis ab obcaecati. Ad eveniet facere ipsam ipsa in labore, incidunt, hic sequi, debitis nostrum tempore.</p>

        <h1>API fetched DATA:</h1>
        <pre>
          {
            JSON.stringify(this.state.data, null, 2)
          }
        </pre>
      </Fragment>
    )
  }
}

export default User;
