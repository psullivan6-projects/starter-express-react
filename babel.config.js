const aliases = require('./webpack/aliases')(__dirname);

module.exports = function (api) {
  if (api != null) {
    api.cache(true);
  }

  const presets = [
    '@babel/preset-env',
    '@babel/preset-react',
  ];
  const plugins = [
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-class-properties',
    'babel-plugin-styled-components',
    [
      "module-resolver", {
        "alias": {
          ...aliases.app,
          ...aliases.server,
        }
      }
    ]
  ];

  return {
    presets,
    plugins
  };
}
